```
Copyright (C) 2013 Alexandre Hannud Abdo <abdo@member.fsf.org>

Project page: https://gitlab.com/solstag/licencas-cc

This work is licensed under the GNU GPL, version 3 or (at your option)
any later version. A copy of the license can be found here:
http://www.gnu.org/licenses/gpl
```
If you can't read Portuguese, see the last section in this page.

# Template Latex para licenças livres Creative Commons

Este é um template simples para incluir uma licença livre dentre as
Licenças Creative Commons na forma de uma página em seu documento latex.

Ele também inclui informação XMP para que o PDF gerado contenha uma
versão legível por máquina da licença.

Deve-se usar 'pdflatex' se quiser incluir a licença legível por máquina.


### Índice:
* Instruções para gerar a página
* Instruções para incluir os metadados XMP
* Instruções finais para gerar o PDF


## Instruções para gerar a página

Copie a pasta 'licencas-cc' para o diretório onde encontra-se seu
documento LaTeX.

Insira o código abaixo no preâmbulo do seu documento LaTeX:

%
% Pacotes necessários para licencas-cc
\usepackage[utf8]{inputenc}
\usepackage{epsfig}
\usepackage[full]{textcomp}
\usepackage[brazil]{babel}
\usepackage{url}
%

Insira o código abaixo logo após as informações catalográficas,
ou onde apropriado, trocando ANO e AUTOR pelos valores correspondentes:

%
% License page
\newpage
\input{licencas-cc/latex/license}
\CcLicenseBySa{pt_BR}{AUTOR}{ANO}
\newpage
%

O código acima usará a licençá CC-BY-SA, mas você pode escolher
qualquer uma dentre as duas licenças livres com atribuição:

CC-BY:     \CcLicenseBy
CC-BY-SA:  \CcLicenseBySa


## Instruções para incluir os metadados XMP

Adquira o pacote xmpincl da CTAN em:

http://ctan.org/tex-archive/macros/latex/contrib/xmpincl

Extraia-o no diretório licencas-cc/latex e então execute num terminal:

$ cd ./licencas-cc/latex/xmpincl
$ tex xmpincl.ins
$ cd -

Adicione os códigos latex abaixo no preâmbulo do seu documento LaTeX:

% Necessários para os metadados XMP de licencas-cc
\usepackage{licencas-cc/latex/xmpincl/xmpincl}
\includexmp{metadata}
%

Agora acesse o gerador de licenças da Creative Commons:

http://creativecommons.org/choose/?lang=pt_BR

Escolha a licença desejada e preencha os seus dados na seção sobre
metadados. No item "Marca da licença", selecione a opção "XMP".
Baixe o arquivo XMP gerado e salve-o como "metadata.xmp" no diretório
onde encontra-se seu documento LaTex.

Alternativamente, ou se não estiver conectado à Internet, você pode
copiar e editar um dos arquivos XMP do diretório
'licencas-cc/latex/exemplos', trocando os detalhes sobre a licença e
substituindo as palavras AUTOR e ANO conforme aplicável.

## Instruções finais para gerar o PDF

Pronto, é hora de rodar:

$ pdflatex SEU_DOCUMENTO.tex

E abrir o resultado num visualizador de PDFs para conferir se a página
gerada está correta, assim como nas propriedades do documento verificar
se os metadados da licença foram incluídos.

Por fim, dependendo do tipo de página e formatação que você usa, podem
ser necessários alguns ajustes finos nos arquivos deste módulo.

Ni!

# English

Sorry, no detailed English instructions yet.

This pseudo-module can generate license pages in Portuguese, and metadata, for all the Free-As-In-Freedom Attribution licenses provided by Creative Commons.

If you're smart you can probably figure out what is going on and adapt it to other languages and situations... contributions are welcome :)
